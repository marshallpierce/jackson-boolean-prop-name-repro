import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

final class JavaTest {
    @Test
    void fieldsShouldUseJsonPropertyAnnotation() throws IOException {
        ObjectMapper mapper = new ObjectMapper();

        String json = mapper.writeValueAsString(new JavaThing("s", true, false));

        List<String> names = new ArrayList<>();
        mapper.readTree(json).fieldNames().forEachRemaining(names::add);
        Collections.sort(names);

        assertEquals(Arrays.asList("booleanWithIsPrefix", "booleanWithoutIsPrefix", "name"), names);
    }

    static class JavaThing {
        @JsonProperty("name")
        private final String stringField;
        @JsonProperty("booleanWithIsPrefix")
        private final boolean isSomething;
        @JsonProperty("booleanWithoutIsPrefix")
        private final boolean whatever;

        JavaThing(String stringField, boolean isSomething, boolean whatever) {
            this.stringField = stringField;
            this.isSomething = isSomething;
            this.whatever = whatever;
        }
    }
}
