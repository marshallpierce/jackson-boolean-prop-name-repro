package org.mpierce.jackson

import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.node.ObjectNode
import com.fasterxml.jackson.module.kotlin.KotlinModule
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class KotlinTest {

    @Test
    internal fun fieldsShouldUseJsonPropertyAnnotation() {
        val mapper = ObjectMapper().apply {
            registerModule(KotlinModule())
        }

        val json = mapper.writeValueAsString(KotlinThing("s", true, false))
        val fieldNames = (mapper.readTree(json) as ObjectNode).fieldNames()
                .asSequence()
                .sorted()
                .toList()

        assertEquals(listOf("booleanWithIsPrefix", "booleanWithoutIsPrefix", "name"), fieldNames)
    }
}

data class KotlinThing(
        @JsonProperty("name") val stringField: String,
        // the field name, sans "is", gets used instead of the @JsonProperty value
        @JsonProperty("booleanWithIsPrefix") val isSomething: Boolean,
        @JsonProperty("booleanWithoutIsPrefix") val whatever: Boolean
)
